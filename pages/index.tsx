import { useState } from "react";
import { 
	Biografi,
	Footer,
	Hero, 
	OffCanvas, 
	SectionContactForm, 
	SectionPortofolio, 
	SectionSkill
} from "../components";

export default function Index() {
	const [isShowed,setShowed] = useState(false);

	return (
		<>
			<Hero
				setShowed={() => setShowed(true)}
			/>
			<Biografi />
			<SectionSkill />
			<SectionPortofolio />
			<SectionContactForm />
			<Footer />
			<OffCanvas
				isShowed={isShowed}
				setShowed={() => setShowed(false)}
			/>
		</>
		
	)
}
