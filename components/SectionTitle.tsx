
interface propsInt {
    children:String,
    className?:string
}
export default function SectionTitle (props:propsInt) {
    const addClassName:string = props.className ? `${props.className}` : 'text-center';
    return (
        <h2 className={`text-2xl font-mono font-semibold ${addClassName} `}>{props.children}</h2>
    )
}