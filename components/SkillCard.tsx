import Image from 'next/image';

interface propsInt {
    Label:String,
    Level:String,
    Image:string
}

export default function SkillCard (props:propsInt) {
    return (
        <div className="bg-white shadow-card flex items-center rounded-lg p-6">
            <Image src={props.Image} width="70%" height="70%" className="rounded" objectFit="contain"  alt="skill-logo"/>
            <div className="ml-4">
                <h4 className="text-lg font-semibold font-mono">{props.Label}</h4>
                <p className="text-sm text-gray-300 font-semibold mt-1">{props.Level}</p>
            </div>
        </div>
    )
}