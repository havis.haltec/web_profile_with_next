import Image from 'next/image';

interface propsInt {
    Label:string,
    Value:string,
    Icon:string
}

export default function ContactItem (props:propsInt) {
    return (
        <div className="flex items-start mb-10">
            <Image src={props.Icon} width="25%" height="25%" objectFit="contain" alt="icon-svg" />
            <div className="ml-5">
                <div className="font-semibold text-sm mb-1">{props.Label}</div>
                <div className="font-semibold text-lg">{props.Value}</div>
            </div>
        </div>
    )
}