interface propsInt {
    label:String;
}

export default function Logo (props:propsInt) {
    return (
        <div className="uppercase text-lg font-semibold tracking-widest text-white">
            {props.label}
        </div> 
    );
}