import { Nav } from ".";
import Image from "next/image";

interface propsInt {
    setShowed:any,
    isShowed:Boolean
}

export default function OffCanvas (props:propsInt) {
    return (
        <>
            <div className={`fixed z-index-999 h-full w-full top-0 bg-white p-10 md:hidden transition-all ${props.isShowed ? '-right-0' : '-right-full' }`}>
                <div className="flex">
                    <div className="w-6/12">
                        <Nav scheme="dark" dir="vertical" />
                    </div>
                    <div className="w-6/12 text-right">
                        <Image alt="close" src="/x-circle.svg" height="30%" width="30%" onClick={props.setShowed} />
                    </div>
                </div>
            </div>
        </>
    )
}