interface PropsInt {
    children:String,
    className?:String,
    Square?:Boolean,
    variant:string,
    href?:string
}

export default function Button ( props:PropsInt ) {
    const addClassName:String = props.className ? `${props.className}` : '';
    const variants:any = {
        'outline-yellow' : `border border-yellow-500 text-yellow-500 hover:text-white hover:bg-yellow-500`,
        'yellow' : `bg-yellow-500 hover:bg-yellow-600 text-black hover:`,
        'black' : `bg-black text-white hover:bg-opacity-90`
    }

    const pickedVariant = variants[props.variant];

    return <a href={props.href} type="button" className={`transition border px-10 py-3 ${props.Square ? '' : 'rounded-full'} font-semibold ${pickedVariant} ${addClassName}`}>{ props.children }</a>
}