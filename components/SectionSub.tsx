interface propsInt {
    children:String,
    className?:string
}

export default function SectionSub (props:propsInt) {
    const addClassName:string = props.className ? `${props.className}` : 'text-center';
    return (
        <p className={`text-lg text-gray-600 mt-2 ${addClassName} `}>{props.children}</p>
    )
}