import Image from "next/image";
import { SectionSub, SectionTitle, SkillCard } from ".";

export default function SectionSkill () {
    return (
        <section className="mt-24 py-20 bg-sectionSkill px-10 2xl:px-0" id="skills">
            <div className="container mx-auto">
                <SectionTitle>Skill</SectionTitle>
                <SectionSub>Some Of My Skill.</SectionSub>
                <div className="flex mt-20 flex-wrap -mx-6 justify-center">
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="Javascript"
                            Level="Lanjutan"
                            Image="/javascript.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="React JS"
                            Level="Menengah"
                            Image="/react.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="Laravel"
                            Level="Lanjutan"
                            Image="/laravel.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="Next JS"
                            Level="Pemula"
                            Image="/nextjs-icon.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="Typescript"
                            Level="Menengah"
                            Image="/typescript-icon.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="Node JS"
                            Level="Pemula"
                            Image="/nodejs.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="CodeIgniter"
                            Level="Lanjutan"
                            Image="/codeigniter.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="React Native"
                            Level="Menengah"
                            Image="/react.svg"
                        />
                    </div>
                    <div className="lg:w-4/12 md:w-6/12 px-6 mb-6 w-full">
                        <SkillCard
                            Label="GIT"
                            Level="Lanjutan"
                            Image="/git-icon.svg"
                        />
                    </div>
                </div>
            </div>
        </section>
    )
}