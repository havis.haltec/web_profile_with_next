export default function Footer () {
    return (
        <footer className="text-gray-400 text-center tracking-wider text-sm font-semibold py-10">
            DESIGNED BY VIZUCODE
        </footer>
    )
}