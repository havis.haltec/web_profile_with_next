import Image from "next/image";
import { SectionSub, SectionTitle } from ".";

export default function Biografi () {
    return (
        <section className="mt-20" id="profile">
            <div className="container mx-auto">
                <SectionTitle>Profile.</SectionTitle>
                <SectionSub>Let`s Know About Me.</SectionSub>
                <div className="flex xl:w-9/12 mx-auto mt-10 lg:items-center md:items-start flex-wrap">
                    <div className="md:w-5/12 lg:p-10 md:p-5 w-full p-10">
                        <Image src="/my-profile.png" alt="my-profile-image" width="100%" height="150%" layout="responsive" objectFit="contain" />
                    </div>
                    <div className="md:w-7/12 px-10 w-full">
                        <p className="text-lg leading-relaxed">
                            Hi, My Name is <span className="font-bold">Havis</span> some people just called <span className="font-bold">Vizu</span>, I live in East Java Indonesia, I`m also Software Engineering Student.
                        </p>
                        <p className="text-lg leading-relaxed mt-5">
                            A Junior Software Engineer, that`s Love to learn continously, Especially about a Software Technology, Like also to discuss a something new Idea to grow a Startup Tech.  
                        </p>
                    </div>
                </div>
            </div>
        </section>
    )
}