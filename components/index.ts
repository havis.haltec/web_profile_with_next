import Nav from "./Nav";
import NavItems from "./NavItems";
import Logo from "./Logo";
import Button from "./Button";
import Navbar from "./Navbar";
import Hero from "./Hero";
import Biografi from "./Biografi";
import SectionSub from "./SectionSub";
import SectionTitle from "./SectionTitle";
import SectionSkill from "./SectionSkill";
import SkillCard from "./SkillCard";
import SectionPortofolio from "./SectionPortofolio";
import SectionArtickle from "./SectionArtickle";
import SectionContactForm from "./SectionContactForm";
import ContactItem from "./ContactItem";
import FieldForm from "./FieldForm";
import Footer from "./Footer";
import OffCanvas from "./OffCanvas";

export {
    Hero,
    Navbar,
    Nav,
    NavItems,
    Logo,
    Button,
    Biografi,
    SectionSub,
    SectionTitle,
    SectionSkill,
    SkillCard,
    SectionPortofolio,
    SectionArtickle,
    SectionContactForm,
    ContactItem,
    FieldForm,
    Footer,
    OffCanvas
}