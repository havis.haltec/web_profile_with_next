import Image from 'next/image';

interface propsInt {
    Header:string,
    Description:string,
    Image:string
}

export default function SectionArtickle (props:propsInt) {
    return (
        <div className="text-center">
            <Image src={ props.Image } width="200" height="150" layout="responsive" className="rounded-xl" alt="portofolio-images" />
            <h3 className="font-semibold text-xl mt-10 mb-1">{ props.Header }</h3>
            <p className="text-gray-400 text-lg mb-10">{ props.Description }</p>
        </div>
    )
}