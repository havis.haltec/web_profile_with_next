import { NavItems } from ".";

interface intProps {
    dir:string,
    scheme:string
}

export default function Nav (props:intProps) {
    
    const dirs:any = {
        horizontal:'justify-center space-x-10',
        vertical: 'flex-col space-y-6'
    }

    const pickedDirs:string = dirs[props.dir];

    return (
        <ul className={`flex ${pickedDirs}`}>
            <NavItems scheme={props.scheme} href="#profile">Profile</NavItems>
            <NavItems scheme={props.scheme} href="#skills">Skill</NavItems>
            <NavItems scheme={props.scheme} href="#portofolio">Projects</NavItems>
            <NavItems scheme={props.scheme} href="#contact">Contact</NavItems>
        </ul>
    )
}