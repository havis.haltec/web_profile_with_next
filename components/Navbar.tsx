import { useState } from "react"
import Image from "next/image"
import { Button, Logo, Nav } from "."

interface propsInt {
    setShowed:any
}

function Navbar(props:propsInt) {

    const [isShowed,setShowed] = useState(false);

    return (
        <>
            <div className="flex items-center md:pb-20 pt-10">
                <div className="w-3/12">
                    <Logo label={'VizuCode'} />
                </div>
                <div className="w-6/12 hidden md:block">
                    <Nav scheme="light" dir="horizontal" />
                </div>
                <div className="w-3/12 text-right hidden md:block">
                    <Button href="#contact" variant="outline-yellow">Contact</Button>
                </div>
                <div className="w-9/12 md:hidden text-right">
                    <Image alt="menu" src="/menu.svg" width="30%" height="30%" onClick={props.setShowed} />
                </div>
            </div>
        </>
    )
}

export default Navbar
