import { Button, ContactItem, FieldForm, SectionSub, SectionTitle } from ".";

export default function SectionContactForm () {
    return (
        <section className="container mx-auto mt-20" id="contact">
            <div className="flex bg-sectionSkill rounded-lg flex-wrap">
                <div className="md:w-6/12 w-full border-r px-10 border-sectionSkill-100">
                    <div className="py-16 lg:px-20">
                        <SectionTitle className="text-left">Contact</SectionTitle>
                        <SectionSub className="text-left mb-20">You Can Find Me At.</SectionSub>
                        <ContactItem 
                            Label="Mail"
                            Value="havisikkubaru@gmail.com"
                            Icon="/mail.svg"
                        />
                        <ContactItem 
                            Label="Phone"
                            Value="0877-08466-279"
                            Icon="/phone.svg"
                        />
                        <ContactItem 
                            Label="Linkedin"
                            Value="linkedin.com/in/vizstars7"
                            Icon="/linkedin.svg"
                        />
                    </div>
                </div>
                <div className="md:w-6/12 w-full">
                    <form className="py-16 lg:px-20 px-10">
                        <div className="flex md:-mx-4 flex-wrap">
                            <div className="md:w-6/12 w-full md:px-4">
                                <FieldForm 
                                    Label="Name"
                                    Name="name"
                                    Type="text"
                                />
                            </div>
                            <div className="md:w-6/12 w-full md:px-4">
                                <FieldForm 
                                    Label="Email"
                                    Name="email"
                                    Type="text"
                                />
                            </div>
                        </div>
                        <FieldForm
                            Label="Subject"
                            Name="subject"
                            Type="text"
                        />
                        <FieldForm
                            Label="Message"
                            Name="message"
                            Type="textarea"
                        />
                        <div className="text-right">
                            <Button variant="black" Square>Send</Button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    )
}