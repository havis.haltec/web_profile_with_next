import { Button, Navbar } from ".";
interface propsInt {
    setShowed:any
}

export default function Hero (props:propsInt) {
    return (
        <div className="bg-hero 2xl:h-[712px] xl:h-[600px] lg:h-[600px] md:h-[500px] pb-10 md:pb-0">
            <div className="container mx-auto px-10 2xl:px-0">
                <Navbar setShowed={props.setShowed} />
                <div className="text-center xl:mt-16 lg:mt-10 pt-10">
                    <h1 className="lg:text-3xl md:text-2xl text-sm text-white font-semibold font-mono 2xl:w-6/12 w-12/12 lg:w-10/12 md:w-11/12 mx-auto leading-relaxed">
                    Hi, My name called Vizu, I`m a Fullstack Web Developer And Mobile Developer. 
                    </h1>
                    <p className="text-white text-sm w-11/12 lg:text-lg md:text-md text-opacity-60 2xl:w-3/12 xl:w-4/12 lg:w-6/12 md:w-6/12 mx-auto mt-6 leading-relaxed">
                        i love to learn any problem and solve with logic and algorythm, likely just a Tech Nerd.
                    </p>
                    <Button href="#profile" variant="yellow" className="inline-block mt-14">Download CV.</Button>
                </div>
            </div>
        </div>
    );
}