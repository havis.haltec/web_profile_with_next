interface PropsInt {
    children:String;
    href:string,
    scheme:string
}

function NavItems(props:PropsInt) {

    const schemes:any = {
        light: "text-white",
        dark: "text-black"
    }

    const pickedScheme:string = schemes[props.scheme];

    const { children } = props;
    return <li><a href={props.href} className={`transition ${pickedScheme} text-lg text-opacity-60 font-semibold hover:text-opacity-100`}>{ children }</a></li>
}

export default NavItems
