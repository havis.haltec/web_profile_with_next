
interface propsInt {
    Label:string,
    Name:string,
    Type?:string
}

export default function FieldForm ( props:propsInt ) {
    return (
        <div className="mb-6">
            <label htmlFor={props.Name} className="block mb-1 text-sm font-semibold">{props.Label}</label>
            {props.Type == 'text' && (
                <input type="text" name={props.Name} id={props.Name} className="bg-transparent border border-sectionSkill-300 py-3 px-6 w-full" />
            )}
            {props.Type == 'textarea' && (
                <textarea name={props.Name} id={props.Name} className="bg-transparent border border-sectionSkill-300 py-3 px-6 w-full h-40" />
            )}
        </div>
    )
}