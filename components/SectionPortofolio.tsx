import { SectionArtickle, SectionSub, SectionTitle } from ".";

export default function SectionPortofolio () {
    return (
        <section>
            <div className="container mx-auto mt-20 px-10 2xl:px-0" id="portofolio">
                <SectionTitle>Portofolio</SectionTitle>
                <SectionSub>My Project That Has Created.</SectionSub>
                <div className="flex -mx-4 mt-20 flex-wrap">
                    <div className="md:w-6/12 w-full md:px-4">
                        <SectionArtickle
                            Header="Hasten"
                            Description="Explorasi Landing Page"
                            Image="/porto1.png"
                        />
                    </div>
                    <div className="md:w-6/12 w-full md:px-4">
                        <SectionArtickle
                            Header="Resources"
                            Description="Explorasi resources page"
                            Image="/porto2.png"
                        />
                    </div>
                    <div className="md:w-6/12 w-full md:px-4">
                        <SectionArtickle
                            Header="Resources"
                            Description="Explorasi resources page"
                            Image="/porto2.png"
                        />
                    </div>
                    <div className="md:w-6/12 w-full md:px-4">
                        <SectionArtickle
                            Header="Hasten"
                            Description="Explorasi Landing Page"
                            Image="/porto1.png"
                        />
                    </div>
                </div>
            </div>
        </section>
    )
}